package com.example.tugas3_danielardenphilo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
    private lateinit var imageViewRambut: ImageView
    private lateinit var imageViewAlis: ImageView
    private lateinit var imageViewKumis: ImageView
    private lateinit var imageViewJanggut: ImageView

    private lateinit var checkBoxRambut: CheckBox
    private lateinit var checkBoxAlis: CheckBox
    private lateinit var checkBoxKumis: CheckBox
    private lateinit var checkBoxJanggut: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageViewRambut = findViewById(R.id.imageViewHair)
        imageViewAlis = findViewById(R.id.imageViewEyebrow)
        imageViewKumis = findViewById(R.id.imageViewMoustache)
        imageViewJanggut = findViewById(R.id.imageViewBeard)

        checkBoxRambut = findViewById(R.id.checkBoxRambut)
        checkBoxAlis = findViewById(R.id.checkBoxAlis)
        checkBoxKumis = findViewById(R.id.checkBoxKumis)
        checkBoxJanggut = findViewById(R.id.checkBoxJanggut)

        checkBoxRambut.setOnCheckedChangeListener { _, isChecked ->
            setVisibility(isChecked, imageViewRambut)
        }

        checkBoxAlis.setOnCheckedChangeListener { _, isChecked ->
            setVisibility(isChecked, imageViewAlis)
        }

        checkBoxKumis.setOnCheckedChangeListener { _, isChecked ->
            setVisibility(isChecked, imageViewKumis)
        }

        checkBoxJanggut.setOnCheckedChangeListener { _, isChecked ->
            setVisibility(isChecked, imageViewJanggut)
        }
    }

    private fun setVisibility(isChecked: Boolean, imageView: ImageView) {
        if (isChecked) {
            imageView.visibility = View.VISIBLE
        } else {
            imageView.visibility = View.INVISIBLE
        }
    }
}